<?php require 'inc/header.php'; ?>
<div class="innerPage">
	<section class="banner innerBanner">
		<div class="bannerContainer" style="background-image: url(images/innerBanner.jpg);">
			<div class="bannerWrapper">
				<div class="container">
					<h1>Privacy</h1>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd">
		<div class="container">
			<p>Muskoka Decks & Railings goes to great measures in protecting your personal privacy. Since we are deeply committed to protecting the security and welfare of all of our clients, our policy is to protect the privacy of every visitor to our company website as well. To assist you in understanding how we collect and use information gathered on our website, we have provided the following overview:</p>

			<h2>Personal Information Provided by Consent</h2>

			<p>Wedo not collect individual identifiable data from users on our website unless you have chosen to provide it to us either via an electronic form or email. All information is kept strictly confidential. There are a number of ways we collect your information on our website. You provide personal information when:</p>

			<ul>
				<li>Sending an Email </li>
				<li>Submitting An Online Request </li>
				<li>Other Possible Forms of Online Communication</li>
			</ul>

			<p>By using any of these services, you automatically consent to the collection and use of your personal information by Muskoka Decks & Railings as described in this policy. It is important to note that you may withdraw your consent at any time simply by emailing us. If you do not give consent, or you later withdraw it, you may not be able to receive certain services that rely on the collection and use of personal information.</p>

			<h2>IP Addresses & Website Traffic</h2>

			<p>When you visit our website, we collect some basic statistical information that does not identify you as an individual user. This includes the amount of traffic visiting the site, which pages are visited and for how long, which search engines the visitors are using to get to our site, and what ISP they are using. We use the information we collect to improve the quality of our web site, enhance or tailor the information we offer, and make your experience on our site as valuable and efficient as possible. None of this information is ever disclosed to third parties. It is kept strictly confidential.</p>

			<h2>Third Party Links & Websites</h2>

			<p>From time to time Muskoka Decks & Railings will provide links to third parties as a courtesy to our visitors. They may have privacy policies that differ from our own. We encourage you to review any privacy policies or notices posted by those service providers.</p>

			<h2>Privacy Policy Modifications</h2>

			<p>Muskoka Decks & Railings may update its privacy policy from time to time. Such changes will be posted on this page. So that you are always aware of what information we collect and how we use it, we recommend that you review this page regularly. Our commitment to confidentiality and adherence to federal law will remain.</p>

			<h2>Questions & More Information</h2>

			<p>If you have questions or concerns about our company privacy policy or data processing, please <a href="#">contact us</a> directly.</p>
			<p>For more information on the federal Personal Information Protection and Electronic Documents Act (PIPEDA), please see the act posted in its entirety on the Federal Privacy Commissioner’s website at <a href="https://www.priv.gc.ca/">www.priv.gc.ca</a></p>
		</div>
	</section>
</div>
<?php require 'inc/footer.php'; ?>