<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=0">
	<title>Muskoka Decks &amp; Railing</title>

	<link rel="icon" type="image/png" href="images/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="images/favicon-16x16.png" sizes="16x16" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="css/style.css">

	<!-- jQuery -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body>
	<header>
		<div class="container">
			<div class="headerContainer">
				<div class="logoContainer">
					<a href="index.php">
						<img src="images/logo.png" alt="Muskoka Decks &amp; Railing">
					</a>
				</div>
				<div class="navContainer">
					<div class="topNav">
						<ul>
							<li>
								<a href="tel:7057834202">(705) 783-4202</a>
							</li>
							<li>
								<a href="mailto:care@easylabz.com">info@MuskokaDecks.ca</a>
							</li>
						</ul>
					</div>
					<div class="mainNav">
						<nav>
							<ul>
								<li class="menu-item current-menu-item"><a href="index.php">Home</a></li>
								<li class="menu-item"><a href="aboutus.php">About Us</a></li>
								<li class="menu-item"><a href="decks.php">Decks</a></li>
								<li class="menu-item"><a href="railing.php">Railing</a></li>
								<li class="menu-item"><a href="gallery.php">Photo Gallery</a></li>
								<li class="menu-item"><a href="contactus.php">Contact Us</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>