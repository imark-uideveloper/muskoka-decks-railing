<?php require 'inc/header.php'; ?>
<div class="index home">
	<section class="banner homeBanner">
		<div class="bannerContainer" style="background-image: url(images/homeBanner.jpg);">
			<div class="bannerWrapper">
				<div class="container">
					<h1>Welcome to Muskoka</h1>
					<h2>Decks &amp; Railing</h2>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd OutdoorLiving">
		<div class="container">
			<div class="innerContent">
				<h2>Outdoor <span>Living</span></h2>

				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue. Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit. Donec aliquam lobortis rhoncus.</p>
				<p>Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac, dignissim at nisi. Fusce laoreet dolor ac fermentum cursus. Donec ultricies dolor faucibus aliquet posuere.</p>
			</div>
		</div>
	</section>

	<section class="sectionPadd bgGray OurRecentWork">
		<div class="container">
			<h2>Our Recent <span>Work</span></h2>

			<div id="ourrecentwork" class="owl-carousel owl-theme">
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork1.jpg);"></figure>
				</div>
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork2.jpg);"></figure>
				</div>
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork3.jpg);"></figure>
				</div>
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork4.jpg);"></figure>
				</div>
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork1.jpg);"></figure>
				</div>
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork2.jpg);"></figure>
				</div>
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork3.jpg);"></figure>
				</div>
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork4.jpg);"></figure>
				</div>
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork1.jpg);"></figure>
				</div>
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork2.jpg);"></figure>
				</div>
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork3.jpg);"></figure>
				</div>
				<div class="item">
					<figure style="background-image: url(images/ourrecentwork4.jpg);"></figure>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd BeforeAfterSection">
		<div class="container">
			<h2>Before and <span>After</span></h2>

			<div id="BeforeAfter" class="owl-carousel owl-theme">
				<div class="item">
					<div class="BeforeAfterWrapper">
						<figure class="figureCOntainer beforeContainer" style="background-image: url(images/before.jpg);"></figure>
						<figure class="figureCOntainer afterContainer" style="background-image: url(images/after.jpg);"></figure>
					</div>
				</div>
				<div class="item">
					<div class="BeforeAfterWrapper">
						<figure class="figureCOntainer beforeContainer" style="background-image: url(images/before.jpg);"></figure>
						<figure class="figureCOntainer afterContainer" style="background-image: url(images/after.jpg);"></figure>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd bgBlack tipsScetion">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6">
					<h3>5 tips for <span>getting started</span></h3>
				</div>
				<div class="col-md-6">
					<div class="innerContent">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis.</p>

						<a class="viewTips" href="#">View Tips</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php require 'inc/footer.php'; ?>