<?php require 'inc/header.php'; ?>
<div class="innerPage">
	<section class="banner innerBanner">
		<div class="bannerContainer" style="background-image: url(images/innerBanner.jpg);">
			<div class="bannerWrapper">
				<div class="container">
					<h1>About Us</h1>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd">
		<div class="container">
			<div class="aboutInnerContainer">
				<div class="aboutCon">
					<h2>About Muskoka Decks &amp; Railing</h2>
					<p>Our team of experienced carpenters have been working throughout Muskoka, Barrie and North Bay for over 18 years. We are now focused more centrally in Muskoka building beautiful decks with crafted rails on Lake Muskoka, Lake Rosseau and Lake Joseph.</p>

					<p>Through the volume of decking we do our broader selection of finishing products provides more choice to you. We learn about our client’s needs and future plans for how they intend to use their deck and then design the perfect deck for them!</p>

					<p>All of our products are made in Canada and are backed by some of the best warranties available in the industry. Choose Muskoka Decks! You’ll be impressed!</p>

					<p><a href="#">Choose Muskoka Decks!</a> You’ll be impressed!</p>
				</div>
				<div class="aboutImg">
					<figure style="background-image: url(images/aboutUs.jpg);"></figure>
				</div>
			</div>
		</div>
	</section>
</div>
<?php require 'inc/footer.php'; ?>