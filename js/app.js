jQuery(document).ready(function($) {
	$('#ourrecentwork').owlCarousel({
		autoplay: true,
		autoplayTimeout: 3000,
		dots: true,
		loop: true,
		margin: 30,
		nav: false,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 3
			},
			1000: {
				items: 4
			}
		}
	});
	$('#BeforeAfter').owlCarousel({
		autoplay: true,
		autoplayTimeout: 3000,
		dots: false,
		items: 1,
		loop: true,
		margin: 0,
		nav: true,
		navText: ['<span>Prev</span><i class="fa fa-angle-left" aria-hidden="true"></i>', '<span>Next</span><i class="fa fa-angle-right" aria-hidden="true"></i>']
	});
});